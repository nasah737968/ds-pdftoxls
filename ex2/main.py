# Pdf to image convert in single page at a time----------

import pypdfium2 as pdfium

filepath = "./ex2/sme.pdf"
pdf = pdfium.PdfDocument(filepath)
# Here give page no
page = pdf.get_page(0)     
pil_image = page.render_to(
    pdfium.BitmapConv.pil_image,
)
pil_image.save("./ex2/convert_img/img1.jpg")
page.close()
pdf.close()
