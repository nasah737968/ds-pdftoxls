# Pdf to excel file convert---------------------------------------------

import PyPDF2
import openpyxl

"""
Example Codes:

pdfFileObj = open('C:/Users/Excel/Desktop/TABLES.pdf', 'rb')
pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
pdfReader.numPages

pageObj = pdfReader.getPage(0)
mytext = pageObj.extractText()


wb = openpyxl.load_workbook('C:/Users/Excel/Desktop/excel.xlsx')
sheet = wb.active
sheet.title = 'MyPDF'
sheet['A1'] = mytext

wb.save('C:/Users/Excel/Desktop/excel.xlsx')
print('DONE!!')

"""

# Note: Read and get pdf data codes-------------------------
pdfFileObj = open('./ex3/test.pdf', 'rb')
pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
pdfReader.numPages
pageObj = pdfReader.getPage(0)
mytext = pageObj.extractText()

# Note: Pdf data pass in excel file and download codes------
wb = openpyxl.load_workbook('./ex3/blank.xlsx')
sheet = wb.active
sheet.title = 'My PDF'
sheet['A1'] = mytext
wb.save('./ex3/exp3.xlsx')
print(sheet)
