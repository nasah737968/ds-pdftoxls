# Pdf to excel file convert---------------------------------------

import tabula
import pandas as pd

# Note: Multiple page pdf to excel convert-------------------------
df = tabula.read_pdf('./ex3/test.pdf', pages = 'all')
for i in range(len(df)):
 df[i].to_excel('./ex3/excel_'+str(i+1)+'.xlsx')

# Note: Direct multiple page pdf to excel convert------------------
tabula.convert_into("./ex3/test2.pdf","./ex3/csv_file.csv", pages = 'all')
