# Step 1--------------------------------------------------------------------------------------

# from pdf2image import convert_from_path
 
# PDF_file = './ex4/smeloan3.pdf'
# pages = convert_from_path(PDF_file, 500, poppler_path=r'C:\Program Files\poppler-0.67.0\bin') 

# image_counter = 1

# for page in pages: 
   
#     filename = "./ex4/img/smeloan3_page_"+str(image_counter)+".jpg"
      
#     # Save the image of the page in system 
#     page.save(filename, 'JPEG') 
  
#     # Increment the counter to update filename 
#     image_counter = image_counter + 1


# Step 2--------------------------------------------------------------------------------------

# import glob
# from PIL import Image
# import pytesseract
# pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
# images = glob.glob("./ex4/img/page_4.jpg")
# for image in images:
#     img = Image.open(image)
#     data = pytesseract.image_to_string(img, lang='eng', config='--psm 6')
#     print(data,file=open('./ex4/csv/page_4.csv',"a"))


# Step 2 updated --------------------------------------------------------------------------------------

import cv2
import pytesseract
import numpy as np

pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

# Load image, grayscale, Otsu's threshold
image = cv2.imread('./ex4/img/smeloan3_page_4.jpg')
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

# Repair horizontal table lines 
kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5,1))
thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel, iterations=1)

# Remove horizontal lines
horizontal_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (55,2))
detect_horizontal = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, horizontal_kernel, iterations=2)
cnts = cv2.findContours(detect_horizontal, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cnts = cnts[0] if len(cnts) == 2 else cnts[1]
for c in cnts:
    cv2.drawContours(image, [c], -1, (255,255,255), 9)

# Remove vertical lines
vertical_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (2,55))
detect_vertical = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, vertical_kernel, iterations=2)
cnts = cv2.findContours(detect_vertical, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cnts = cnts[0] if len(cnts) == 2 else cnts[1]
for c in cnts:
    cv2.drawContours(image, [c], -1, (255,255,255), 9)

# Perform OCR
data = pytesseract.image_to_string(image, lang='eng',config='--psm 6')
print(data,file=open('./ex4/csv/smeloan3_page_4.csv',"a"))

